package com.rc.apps.messanger.logic.services;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.repository.AppUserRepository;
import com.rc.apps.messanger.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AppUserService {

	@Autowired
	private AppUserRepository appUserRepository;

	@Autowired
	private ContactRepository contactRepository;

	public List<AppUser> getAllUsers() {
		return appUserRepository.findAll(new Sort("userName"));
	}

    @Transactional
	public AppUser getUserByEmail(String email) {
		return appUserRepository.findAppUserByEmail(email);
	}

	@Transactional
	public List<AppUser> storeAppUser(AppUser appUser) {
		appUserRepository.saveAndFlush(appUser);
        Contact contact = new Contact();
        contact.setUser(appUser);
        contactRepository.saveAndFlush(contact);
		return appUserRepository.findAll(new Sort("userName")); //TODO: extract to private method
	}

}
