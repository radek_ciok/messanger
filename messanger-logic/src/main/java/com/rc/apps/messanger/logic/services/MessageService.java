package com.rc.apps.messanger.logic.services;

import com.rc.apps.messanger.domain.Message;
import com.rc.apps.messanger.domain.MessageHistory;
import com.rc.apps.messanger.domain.MessageQueue;
import com.rc.apps.messanger.repository.MessageHistoryRepository;
import com.rc.apps.messanger.repository.MessageQueueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageQueueRepository messageQueueRepository;

    @Autowired
    private MessageHistoryRepository messageHistoryRepository;

    @Transactional
    public Message saveMessage(Message message) {
        Message m = null;
        if (message instanceof MessageQueue) {
            m = messageQueueRepository.saveAndFlush((MessageQueue) message);
        } else if (message instanceof MessageHistory) {
            m = messageHistoryRepository.saveAndFlush((MessageHistory) message);
        }
        return m;
    }

    @Transactional
    public Message moveMessageFromQueueToHistory(MessageQueue messageQueue) {
        MessageHistory messageHistory = messageQueueToHistory(messageQueue);
        MessageHistory storedMessageHistory = messageHistoryRepository.saveAndFlush(messageHistory);
        messageQueueRepository.delete(messageQueue);
        return storedMessageHistory;
    }

    @Transactional
    public List getAllMessageQueues() {
        return messageQueueRepository.findAll();
    }

    // TODO: make it generic
    @Transactional
    public void deleteMessageQueue(Message message) {
        messageQueueRepository.delete((MessageQueue) message);
    }

    private MessageHistory messageQueueToHistory(MessageQueue messageQueue) {
        MessageHistory messageHistory = new MessageHistory();
        messageHistory.setRecipients(messageQueue.getRecipients());
        messageHistory.setText(messageQueue.getText());
        messageHistory.setIncomeTime(messageQueue.getIncomeTime());
        messageHistory.setInoutTime(messageQueue.getInoutTime());
        return messageHistory;
    }

}
