package com.rc.apps.messanger.logic.exceptions;

public class ResourceNotFoundException extends RuntimeException {

	private String resourceIdentifier;
	
	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException(String resourceIdentifier) {
		this.resourceIdentifier = resourceIdentifier;
	}
	
	@Override
	public String getMessage() {
		return "Resource [" + resourceIdentifier + "] not found";
	}

	public String getResourceIdentifier() {
		return resourceIdentifier;
	}
	
}
