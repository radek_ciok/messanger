package com.rc.apps.messanger.logic.services;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.repository.AppUserRepository;
import com.rc.apps.messanger.repository.ContactRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContactService {

	@Autowired
	private AppUserRepository appUserRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Transactional
	public List<Contact> getAllContactsOfUser(String email) {
	    return appUserRepository.findAppUserByEmail(email).getContacts();
	}

	@Transactional
	public List<Contact> addContactToUser(String userEmail, String userEmailToAdd) {
		AppUser user = appUserRepository.findAppUserByEmail(userEmail);
        Contact contactToAdd = contactRepository.findContactByEmail(userEmailToAdd);
        user.getContacts().add(contactToAdd);
        return appUserRepository.saveAndFlush(user).getContacts();
	}

	@Transactional
	public List<Contact> removeContactFromUser(String userEmail, String userEmailToRemove) {
		AppUser user = appUserRepository.findAppUserByEmail(userEmail);
        removeContactByEmail(user.getContacts(), userEmailToRemove);
		return appUserRepository.saveAndFlush(user).getContacts();
	}

	private void removeContactByEmail(List<Contact> contacts, String email) {
        contacts.removeIf(contact -> contact.getEmail().equals(email));
	}

}
