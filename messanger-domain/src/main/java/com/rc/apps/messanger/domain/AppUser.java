package com.rc.apps.messanger.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

//@Data
@Entity
@Table(name="APP_USER")
public class AppUser {

	@Id
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "sequence-generator"
	)
	@SequenceGenerator(
			name = "sequence-generator",
			sequenceName = "app_user_sequence",
			allocationSize = 1,
			initialValue = 100
	)
	private Long id;
	
	private String userName;
	
	private String email;
	
	private String name;
	
	private String surname;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Contact> contacts = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<Contact> getContacts() {
		contacts.sort((Contact c1, Contact c2) -> c1.getUserName().compareTo(c2.getUserName()));
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
	    if (contacts == null) {
	        this.contacts = new ArrayList<>();
        } else {
            this.contacts = contacts;
        }
	}

	public void addContacts(Contact... c) {
		getContacts().addAll(Arrays.asList(c));
	}
    public void addContacts(Collection<Contact> contacts) {
        getContacts().addAll(contacts);
    }
}
