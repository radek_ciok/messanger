package com.rc.apps.messanger.domain;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class Message {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence-generator"
    )
    @SequenceGenerator(
            name = "sequence-generator",
            sequenceName = "message_sequence",
            allocationSize = 1,
            initialValue = 100
    )
    private Long id;

    private String text;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date incomeTime;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date inoutTime;

    private String recipients;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getIncomeTime() {
        return incomeTime;
    }

    public void setIncomeTime(Date incomeTime) {
        this.incomeTime = incomeTime;
    }

    public Date getInoutTime() {
        return inoutTime;
    }

    public void setInoutTime(Date inoutTime) {
        this.inoutTime = inoutTime;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

}
