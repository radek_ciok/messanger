package com.rc.apps.messanger.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Contact implements Comparable<Contact> {

    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "sequence-generator"
    )
    @SequenceGenerator(
            name = "sequence-generator",
            sequenceName = "contact_sequence",
            allocationSize = 1,
            initialValue = 100
    )
    private Long id;

    public Contact(){
        super();
    }

    public Contact(AppUser appUser){
        this.user = appUser;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;
    }

    @OneToOne(fetch = FetchType.EAGER)
    private AppUser user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return user.getUserName();
    }

    public String getEmail() {
        return user.getEmail();
    }

    public String getName() {
        return user.getName();
    }

    public String getSurname() {
        return user.getSurname();
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", userName='" + getUserName() + '\'' +
                ", email='" + getEmail() + '\'' +
                ", name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                '}';
    }

    @Override
    public int compareTo(Contact o) {
        return getEmail().compareTo(o.getEmail());
    }
}
