package com.rc.apps.messanger.domain;

public class Health {

	private Boolean isAlive;

	public Boolean getIsAlive() {
		return isAlive;
	}

	public void setIsAlive(Boolean isAlive) {
		this.isAlive = isAlive;
	}

	@Override
	public String toString() {
		return "Health [isAlive=" + isAlive + "]";
	}

}
