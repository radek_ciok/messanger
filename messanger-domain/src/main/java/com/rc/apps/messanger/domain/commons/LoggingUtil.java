package com.rc.apps.messanger.domain.commons;

import java.util.List;

public class LoggingUtil {

    public static <T> void printSpacedList(String title, List<T> list) {
        printMessage("\n");
        printList(title, list);
        printMessage("\n");
    }

    public static <T> void printList(String title, List<T> list) {
        printMessage(title);
        printList(list);
    }

    public static <T> void printList(List<T> list) {
        for (T e : list) {
            printMessage(e.toString());
        }
    }

    public static void printMessage(String message) {
        System.out.println(message);
    }


}
