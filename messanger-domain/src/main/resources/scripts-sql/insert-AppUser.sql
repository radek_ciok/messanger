insert into APP_USER values (1, 'Paulina.Sobczak@o2.com'         , 'Paulina', 'Sobczak'     , 'Paulina Sobczak' );
insert into APP_USER values (2, 'Henryk.Bak@gmail.com'              , 'Henryk', 'Bak'          , 'Henryk_Bak' );           
insert into APP_USER values (3, 'Kazimierz.Gorski@o2.com'        , 'Kazimierz', 'Gorski'    , 'Kazimierz Gorski' );     
insert into APP_USER values (4, 'Irena.Wojcik@onet.pl'  	      , 'Irena', 'Wojcik'        , 'Irena_Wojcik' );         
insert into APP_USER values (5, 'Marian.Chmielewski@gmail.com'      , 'Marian', 'Chmielewski'  , 'Marian__Chmielewski' );   
insert into APP_USER values (7, 'Ewa_Duda@onet.pl'                , 'Ewa', 'Duda'            , 'Ewa.Duda' );             
insert into APP_USER values (8, 'Jakub_Malinowski@gmail.com'        , 'Jakub', 'Malinowski'    , 'Jakub..Malinowski' );     
insert into APP_USER values (9, 'Jadwiga-Brzezinska@gmail.com'      , 'Jadwiga', 'Brzezinska'  , 'Jadwiga-Brzezinska' );   
insert into APP_USER values (10, 'Roman-Sawicki@gmail.com'           , 'Roman', 'Sawicki'       , 'Roman+Sawicki' );        