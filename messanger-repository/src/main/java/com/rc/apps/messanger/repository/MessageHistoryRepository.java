package com.rc.apps.messanger.repository;

import com.rc.apps.messanger.domain.MessageHistory;
import com.rc.apps.messanger.domain.MessageQueue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageHistoryRepository extends JpaRepository<MessageHistory, Long>{

}
