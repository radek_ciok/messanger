package com.rc.apps.messanger.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import com.rc.apps.messanger.domain.AppUser;

import java.util.List;

public interface AppUserRepository extends JpaRepository<AppUser, Long>{

	@EntityGraph(attributePaths = { "contacts" })
	public AppUser findAppUserByEmail(String email);

	@Override
	@EntityGraph(attributePaths = { "contacts" })
	List<AppUser> findAll();

    @Override
    @EntityGraph(attributePaths = { "contacts" })
    List<AppUser> findAll(Sort sort);
}
