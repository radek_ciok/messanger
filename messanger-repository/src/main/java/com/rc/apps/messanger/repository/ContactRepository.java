package com.rc.apps.messanger.repository;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long>{

    @Query("select c from Contact c join AppUser u on c.user.id = u.id where u.email = :email")
    public Contact findContactByEmail(@Param("email") String email);

}
