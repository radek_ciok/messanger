package com.rc.apps.messanger.repository;

import com.rc.apps.messanger.domain.Message;
import com.rc.apps.messanger.domain.MessageQueue;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageQueueRepository extends JpaRepository<MessageQueue, Long>{

}
