insert into APP_USER values (1, 'Ewa_Duda@onet.pl'                , 	'Ewa', 		'Duda'            , 	'Ewa Duda' 				);
insert into APP_USER values (2, 'Roman-Sawicki@o2.pl'           , 		'Roman', 	'Sawicki'       , 		'Roman+Sawicki' 		);
insert into APP_USER values (3, 'Marian.Chmielewski@gmail.com'      , 	'Marian', 	'Chmielewski'  , 		'Marian__Chmielewski' 	);
insert into APP_USER values (4, 'Jakub_Malinowski@gmail.com'        , 	'Jakub', 	'Malinowski'    , 		'Jakub..Malinowski' 	);

insert into CONTACT values (1, 1);
insert into CONTACT values (2, 2);
insert into CONTACT values (3, 3);
insert into CONTACT values (4, 4);

insert into APP_USER_CONTACT values (1, 2); -- Ewa_Duda@onet.pl - Roman-Sawicki@o2.pl
insert into APP_USER_CONTACT values (1, 3); -- Ewa_Duda@onet.pl - Marian.Chmielewski@gmail.com
insert into APP_USER_CONTACT values (2, 1); -- Roman-Sawicki@o2.pl - Ewa_Duda@onet.pl
insert into APP_USER_CONTACT values (2, 3); -- Roman-Sawicki@o2.pl - Marian.Chmielewski@gmail.com
insert into APP_USER_CONTACT values (3, 2); -- Marian.Chmielewski@gmail.com - Roman-Sawicki@o2.pl