package com.rc.apps.messanger.web.rest_services;

import com.rc.apps.messanger.web.TestUtils;
import com.rc.apps.messanger.web.WebIntegrationTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("channel-db")
public class Messaging_dbChannel_Test extends WebIntegrationTest {

    private static final Map<String, String> expectedMessage = new HashMap<String, String>();

    @PersistenceContext(name = "test-messangerPU")
    private EntityManager em;

    static {
        expectedMessage.put("$.text", "sssssss");
        //expectedMessage.put("$.sendTime", "1515841038682");
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-messages.sql"
    })
    public void test_sendReceiveMessage() throws Exception {
        System.out.println("test_sendMessage()");

        String json = TestUtils.getJsonFromFile("message.json");

        ResultActions ra = this.mockMvc.perform(
                post("/message").content(json)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));


        MvcResult jsonResunt = addResultItemsFromMap(ra, expectedMessage).andReturn();
        System.out.println("\n\nresult:\n\n" + jsonResunt.getResponse().getContentAsString() + "\n\n");

        assertMessageDBContent(MSG_QUEUE_TABLE);

        System.out.println("waiting 5s until message is received");
        Thread.sleep(2000);
        System.out.println("stops waiting, finishes test");

        //test message received...

        assertMessageDBContent(MSG_HISTORY_TABLE);

    }

    private void assertMessageDBContent(String msgSourceTable) {
        List messages = getAllMessagesFrom(msgSourceTable);

        assertEquals(1, messages.size());

        Object[] msg = (Object[]) messages.get(0);

        assertEquals("sssssss", msg[0]);
        assertEquals("aaa;bbb;ccc", msg[1]);
    }

    private final String MSG_QUEUE_TABLE = "MESSAGE_QUEUE";
    private final String MSG_HISTORY_TABLE = "MESSAGE_HISTORY";

    private List getAllMessagesFrom(String msgSourceTable) {
        return em.createNativeQuery("SELECT text, recipients FROM " + msgSourceTable).getResultList();
    }


}