package com.rc.apps.messanger.logic.services;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.repository.AppUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static com.rc.apps.messanger.logic.services.ContactServiceTest.UserData.*;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

    @InjectMocks
    private  ContactService contactService = new ContactService();

    @Mock
    private AppUserRepository appUserRepository;

    private AppUser ewaDuda;

    private Map<UserData, Contact> testContacts;

    {
        testContacts = new HashMap<>();
        testContacts.put(ROMAN, createContactWithUser(ROMAN));
        testContacts.put(MARIAN, createContactWithUser(MARIAN));
        testContacts.put(JAKUB, createContactWithUser(JAKUB));

        ewaDuda = createTestUserWithContactList(EWA, testContacts.values());
    }

    @Test
    public void test_removeContactFromUser() {

        when(appUserRepository.findAppUserByEmail(ewaDuda.getEmail())).thenReturn(ewaDuda);
        when(appUserRepository.saveAndFlush(ewaDuda)).thenReturn(ewaDuda);

        List<Contact> contacts = null;

        contacts = contactService.removeContactFromUser(ewaDuda.getEmail(), ROMAN.getEmail());
        assertUsersContactList(contacts,testContacts.get(MARIAN), testContacts.get(JAKUB));

        contacts = contactService.removeContactFromUser(ewaDuda.getEmail(), JAKUB.getEmail());
        assertUsersContactList(contacts, testContacts.get(MARIAN));
    }

    private AppUser createAppUser(UserData userData) {
        AppUser user = new AppUser();
        user.setEmail(userData.getEmail());
        user.setUserName(userData.getUserName());
        return user;
    }

    private Contact createContactWithUser(UserData userData) {
        return new Contact(createAppUser(userData));
    }

    private AppUser createTestUserWithContactList(UserData userData, Collection<Contact> contacts) {
        AppUser user = createAppUser(userData);
        user.addContacts(contacts);
        return user;
    }

    private void assertUsersContactList(List<Contact> contacts, Contact... expContacts) {
        assertEquals(expContacts.length, contacts.size());
        for (Contact c : expContacts) {
            assertTrue(contacts.contains(c));
        }
    }

    enum UserData {
        EWA("Ewa_Duda@onet.pl"), ROMAN("Roman-Sawicki@o2.pl"), MARIAN("Marian.Chmielewski@gmail.com"), JAKUB("Jakub_Malinowski@gmail.com");

        private String email;
        private String userName;

        private UserData(String email) {
            this.email = email;
            this.userName = createTestUserNameFromEmail(email);
        }

        private String createTestUserNameFromEmail(String email) {
            return email.substring(0, email.indexOf("@"));
        }

        public String getEmail() {
            return email;
        }

        public String getUserName() {
            return userName;
        }

    }

}