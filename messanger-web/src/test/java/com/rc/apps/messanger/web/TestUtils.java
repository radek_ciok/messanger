package com.rc.apps.messanger.web;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestUtils {

    private final static String JSON_ROOT = "test-jsons/";

    public static String getJsonFromFile(String fileName) {
        String json = null;
        try {
            URI uri = TestUtils.class.getClassLoader().getResource(JSON_ROOT + fileName).toURI();
            byte[] fileBytes = Files.readAllBytes(Paths.get(uri));
            json = new String(fileBytes);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

}
