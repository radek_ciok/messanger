package com.rc.apps.messanger.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rc.apps.messanger.messaging.model.JsonMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.rc.apps.messanger.web.test_config.TestWebConfig;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = TestWebConfig.class)
public class WebIntegrationTest {
	
    @Autowired
    private WebApplicationContext wac;

    protected MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    protected ResultActions addResultItemsFromMap(ResultActions resultActions, Map<String, String> expectedResultItems) throws Exception {
        for (Map.Entry<String, String> entry : expectedResultItems.entrySet()) {
            resultActions.andExpect(jsonPath(entry.getKey()).value(entry.getValue()));
        }
        return resultActions;
    }

    protected void testGetListRequest_OK (String url, Map<String, String> expectedResults) throws Exception {
        ResultActions ra = this.mockMvc.perform(get(url)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        MvcResult jsonResunt = addResultItemsFromMap(ra, expectedResults).andReturn();

        System.out.println("\n\nresult:\n\n" + jsonResunt.getResponse().getContentAsString() + "\n\n");

    }

    @Test
    public void testOK() {

        ObjectMapper mapper = new ObjectMapper();

        JsonMessage m = new JsonMessage();
        m.setText("sssssss");
        m.setReceiveTime(new Date());
        m.setSendTime(new Date());
        m.setRecipients(Arrays.asList("aaa", "bbb", "ccc"));

        try {
            System.out.println(mapper.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
