package com.rc.apps.messanger.web.rest_services;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.domain.commons.LoggingUtil;
import com.rc.apps.messanger.web.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.rc.apps.messanger.web.WebIntegrationTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import static org.junit.Assert.*;


public class AppUserRestServiceTest extends WebIntegrationTest {

    @PersistenceContext(name = "test-messangerPU")
    private EntityManager em;

	private final static Logger LOG = LoggerFactory.getLogger(AppUserRestServiceTest.class);
	
	private static final Map<String, String> expectedAppUsers = new HashMap<String, String>();
	private static final Map<String, String> expectedAppUsersAfterStore = new HashMap<String, String>();
	private static final Map<String, String> expectedSingleAppUser = new HashMap<String, String>();
	private static final Map<String, String> expectedDataNotFound = new HashMap<String, String>();
	
	static {
		expectedAppUsers.put("$[0].email", 		"Ewa_Duda@onet.pl");
		expectedAppUsers.put("$[0].name", 		"Ewa");
		expectedAppUsers.put("$[0].surname", 	"Duda");
		expectedAppUsers.put("$[0].userName", 	"Ewa Duda");
		
		expectedAppUsers.put("$[1].email", 		"Jakub_Malinowski@gmail.com"	);       
		expectedAppUsers.put("$[1].name", 		"Jakub" 	               	);
		expectedAppUsers.put("$[1].surname", 	"Malinowski" 		   	);
		expectedAppUsers.put("$[1].userName", 	"Jakub..Malinowski" 	 	);     
        
		expectedAppUsers.put("$[2].email", 		"Marian.Chmielewski@gmail.com"	);       
		expectedAppUsers.put("$[2].name", 		"Marian" 	                 	);
		expectedAppUsers.put("$[2].surname", 	"Chmielewski"   		     	);
		expectedAppUsers.put("$[2].userName", 	"Marian__Chmielewski" 	     	);     
        
		expectedAppUsers.put("$[3].email", 		"Roman-Sawicki@o2.pl"	);       
		expectedAppUsers.put("$[3].name", 		"Roman"					);
		expectedAppUsers.put("$[3].surname", 	"Sawicki"       		);
		expectedAppUsers.put("$[3].userName", 	"Roman+Sawicki" 		);     
		
		
		expectedAppUsersAfterStore.put("$[0].email", 		"Ewa_Duda@onet.pl");
		expectedAppUsersAfterStore.put("$[0].name", 		"Ewa");
		expectedAppUsersAfterStore.put("$[0].surname", 	"Duda");
		expectedAppUsersAfterStore.put("$[0].userName", 	"Ewa Duda");

		expectedAppUsersAfterStore.put("$[1].email", 		"Jakub_Malinowski@gmail.com"	);       
		expectedAppUsersAfterStore.put("$[1].name", 		"Jakub" 	               	);
		expectedAppUsersAfterStore.put("$[1].surname", 	"Malinowski" 		   	);
		expectedAppUsersAfterStore.put("$[1].userName", 	"Jakub..Malinowski" 	 	);     

		expectedAppUsersAfterStore.put("$[2].email", 		"Marian.Chmielewski@gmail.com"	);       
		expectedAppUsersAfterStore.put("$[2].name", 		"Marian" 	                 	);
		expectedAppUsersAfterStore.put("$[2].surname", 	"Chmielewski"   		     	);
		expectedAppUsersAfterStore.put("$[2].userName", 	"Marian__Chmielewski" 	     	);     

		expectedAppUsersAfterStore.put("$[3].email", 		"Roman-Sawicki@o2.pl"	);       
		expectedAppUsersAfterStore.put("$[3].name", 		"Roman"					);
		expectedAppUsersAfterStore.put("$[3].surname", 		"Sawicki"       		);
		expectedAppUsersAfterStore.put("$[3].userName", 	"Roman+Sawicki" 		);     

		expectedAppUsersAfterStore.put("$[4].email", 		"test.user@o2.pl"		);       
		expectedAppUsersAfterStore.put("$[4].name", 		"Test"					);
		expectedAppUsersAfterStore.put("$[4].surname", 		"User"       			);
		expectedAppUsersAfterStore.put("$[4].userName", 	"Test_User_1" 			);     

		
		expectedSingleAppUser.put("$.email", 		"Ewa_Duda@onet.pl");
		expectedSingleAppUser.put("$.name", 		"Ewa");
		expectedSingleAppUser.put("$.surname", 		"Duda");
		expectedSingleAppUser.put("$.userName", 	"Ewa Duda");
		
		expectedDataNotFound.put("$.resourceIdentifier", 	"john.doe@o2.pl");

	}

    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-AppUser.sql",
            "classpath:test-scripts-sql/test-insert-AppUser.sql"
    })
    public void test_getAllAppUsers_OK() throws Exception {
    	LOG.info("test_getAllAppUsers_OK()");
    	testGetListRequest_OK("/allAppUsers", expectedAppUsers);
    }
	
    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-AppUser.sql"
    })
    public void test_getAllAppUsers_NoDataFound() throws Exception {
    	LOG.info("test_getAllAppUsers_NoDataFound()");
    	MvcResult result = this.mockMvc.perform(get("/allAppUsers")
                .accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(jsonPath("$", hasSize(0)))
            .andReturn();
        
        System.out.println("\n\nresult:\n\n" + result.getResponse().getContentAsString() + "\n\n");
    }

    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
					"classpath:test-scripts-sql/test-delete-AppUser.sql",
					"classpath:test-scripts-sql/test-insert-AppUser.sql"
    })
    public void test_storeSingleAppUser_OK() throws Exception {
    	LOG.info("test_storeSingleAppUser_OK()");

    	String json = TestUtils.getJsonFromFile("user.json");

		System.out.println("\n\njson from file:\n\n" + json + "\n\n");
    	
    	ResultActions ra = this.mockMvc.perform(
    			post("/appUser").content(json)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
            
    	MvcResult jsonResunt = addResultItemsFromMap(ra, expectedAppUsersAfterStore).andReturn();

    	List l = getContactsOfUser("test.user@o2.pl");

        assertEquals(1, l.size());
        Object[] result = (Object[]) l.get(0);
        assertEquals(result[0], result[1]);
        assertEquals(result[1], result[2]);

    	System.out.println("\n\nresult:\n\n" + jsonResunt.getResponse().getContentAsString() + "\n\n");    
    }
    
    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-AppUser.sql",
            "classpath:test-scripts-sql/test-insert-AppUser.sql"
    })
    public void test_getSingleAppUser_dataExists_OK() throws Exception {
    	LOG.info("test_getSingleAppUser_dataExists_OK()");
    	testGetListRequest_OK("/appUser?email=Ewa_Duda@onet.pl", expectedSingleAppUser);
	}

    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-AppUser.sql",
            "classpath:test-scripts-sql/test-insert-AppUser.sql"
    })
    public void test_getSingleAppUser_dataNotExists_NotFoundException() throws Exception {
    	LOG.info("test_getSingleAppUser_dataNotExists_NotFoundException()");
    	ResultActions ra = this.mockMvc.perform(get("/appUser?email=john.doe@o2.pl")
                .accept(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isNotFound())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
            
    	MvcResult jsonResunt = addResultItemsFromMap(ra, expectedDataNotFound).andReturn();
    	
    	System.out.println("\n\nresult:\n\n" + jsonResunt.getResponse().getContentAsString() + "\n\n");    	    	
    }

    //TODO: refactore to single util
    private List getContactsOfUser(String email) {
	    String query =
                "select c.ID as contact_id, c.USER_ID as contact_USER_ID, u.ID as user_id " +
                        "from APP_USER u join CONTACT c on u.id = c.USER_ID " +
                        "where u.EMAIL = :email";
	    Query q = em.createNativeQuery(query);
	    q.setParameter("email", email);
	    return q.getResultList();
    }
    
}
