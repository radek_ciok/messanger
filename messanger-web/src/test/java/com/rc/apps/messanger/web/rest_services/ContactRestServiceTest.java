package com.rc.apps.messanger.web.rest_services;

import com.rc.apps.messanger.web.WebIntegrationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.ResultActions;


import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import com.rc.apps.messanger.web.WebIntegrationTest;
import org.springframework.transaction.PlatformTransactionManager;


import javax.transaction.TransactionManager;

import static org.junit.Assert.*;

public class ContactRestServiceTest extends WebIntegrationTest {

    private final static Logger LOG = LoggerFactory.getLogger(ContactRestServiceTest.class);

    private static final Map<String, String> expectedContacts = new HashMap<String, String>();
    private static final Map<String, String> expectedContactsAfterRemove = new HashMap<String, String>();
    private static final Map<String, String> expectedUserContactsAfterStore = new HashMap<String, String>();

    static {
        expectedContacts.put("$[0].email", "Marian.Chmielewski@gmail.com");
        expectedContacts.put("$[0].name", "Marian");
        expectedContacts.put("$[0].surname", "Chmielewski");
        expectedContacts.put("$[0].userName", "Marian__Chmielewski");

        expectedContacts.put("$[1].email", "Roman-Sawicki@o2.pl");
        expectedContacts.put("$[1].name", "Roman");
        expectedContacts.put("$[1].surname", "Sawicki");
        expectedContacts.put("$[1].userName", "Roman+Sawicki");

        expectedUserContactsAfterStore.put("$[0].email", "Jakub_Malinowski@gmail.com");
        expectedUserContactsAfterStore.put("$[0].name", "Jakub");
        expectedUserContactsAfterStore.put("$[0].surname", "Malinowski");
        expectedUserContactsAfterStore.put("$[0].userName", "Jakub..Malinowski");

        expectedUserContactsAfterStore.put("$[1].email", "Marian.Chmielewski@gmail.com");
        expectedUserContactsAfterStore.put("$[1].name", "Marian");
        expectedUserContactsAfterStore.put("$[1].surname", "Chmielewski");
        expectedUserContactsAfterStore.put("$[1].userName", "Marian__Chmielewski");

        expectedUserContactsAfterStore.put("$[2].email", "Roman-Sawicki@o2.pl");
        expectedUserContactsAfterStore.put("$[2].name", "Roman");
        expectedUserContactsAfterStore.put("$[2].surname", "Sawicki");
        expectedUserContactsAfterStore.put("$[2].userName", "Roman+Sawicki");

        expectedContacts.put("$[0].email", "Marian.Chmielewski@gmail.com");
        expectedContacts.put("$[0].name", "Marian");
        expectedContacts.put("$[0].surname", "Chmielewski");
        expectedContacts.put("$[0].userName", "Marian__Chmielewski");

        expectedContactsAfterRemove.put("$[0].email", "Marian.Chmielewski@gmail.com");
        expectedContactsAfterRemove.put("$[0].name", "Marian");
        expectedContactsAfterRemove.put("$[0].surname", "Chmielewski");
        expectedContactsAfterRemove.put("$[0].userName", "Marian__Chmielewski");

    }

    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-AppUser.sql",
            "classpath:test-scripts-sql/test-insert-AppUser.sql"
    })
    public void test_getAllContactsForUser() throws Exception {
        LOG.info("test_getAllContactsForUser()");
        testGetListRequest_OK("/allContactsOfUser?userEmail=Ewa_Duda@onet.pl", expectedContacts);
    }

    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {
            "classpath:test-scripts-sql/test-delete-AppUser.sql",
            "classpath:test-scripts-sql/test-insert-AppUser.sql"
    })
    public void test_addContactToUser() throws Exception {
        LOG.info("test_addContactToUser()");

        String userToAdd = "Jakub_Malinowski@gmail.com";

        ResultActions ra = this.mockMvc.perform(
                post("/contact?userEmail=Ewa_Duda@onet.pl").content(userToAdd).contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        MvcResult jsonResunt = addResultItemsFromMap(ra, expectedUserContactsAfterStore).andReturn();

        System.out.println("\n\nresult:\n\n" + jsonResunt.getResponse().getContentAsString() + "\n\n");
    }

    @Test
    public void test_removeContactFromUser() throws Exception {
        // from Ewa_Duda@onet.pl remove Roman-Sawicki@o2.pl
        LOG.info("test_removeContactFromUser()");

        String userToRemove = "Roman-Sawicki@o2.pl";

        ResultActions ra = this.mockMvc.perform(
                delete("/contact?userEmail=Ewa_Duda@onet.pl").content(userToRemove).contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        MvcResult jsonResunt = addResultItemsFromMap(ra, expectedContactsAfterRemove).andReturn();

        System.out.println("\n\nresult:\n\n" + jsonResunt.getResponse().getContentAsString() + "\n\n");
        //Ewa_Duda@onet.pl has only 1 contact = Marian.Chmielewski@gmail.com
        //Roman-Sawicki@o2.pl still exists in appUsers and contacts
    }

}