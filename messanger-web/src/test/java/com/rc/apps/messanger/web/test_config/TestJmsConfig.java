package com.rc.apps.messanger.web.test_config;

import org.hornetq.jms.server.embedded.EmbeddedJMS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;

@Configuration
@EnableJms
@ComponentScan(basePackages = "com.rc.apps.messanger.messaging")
@PropertySource("classpath:messaging.properties")
public class TestJmsConfig {

    @Value("${jms.queue.1}")
    public String desinationMessageQueue1;

    @Bean
    public ConnectionFactory connectionFactory() throws Exception {
        EmbeddedJMS embeddedJMS = new EmbeddedJMS();
        embeddedJMS.start();
        ConnectionFactory cf = (ConnectionFactory)embeddedJMS.lookup("ConnectionFactory");
        return cf;
    }

    @Bean
    @Autowired
    public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory){
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory);
        template.setDefaultDestinationName(desinationMessageQueue1);
        return template;
    }

    @Bean
    @Autowired
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(ConnectionFactory connectionFactory) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setConcurrency("1-1");
        return factory;
    }
}
