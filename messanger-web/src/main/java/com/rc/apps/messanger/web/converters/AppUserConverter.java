package com.rc.apps.messanger.web.converters;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.web.json_model.JsonAppUser;
import com.rc.apps.messanger.web.json_model.JsonContact;

public class AppUserConverter extends PojoConverter<AppUser, JsonAppUser> {

	private ContactConverter contactConverter = new ContactConverter();

	@Override
	public JsonAppUser convertToJson(AppUser appUser) {
		JsonAppUser jsonAppUser = new JsonAppUser();
		jsonAppUser.setEmail(appUser.getEmail());
		jsonAppUser.setUserName(appUser.getUserName());
		jsonAppUser.setName(appUser.getName());
		jsonAppUser.setSurname(appUser.getSurname());
		jsonAppUser.setContacts(contactConverter.toJsonsList(appUser.getContacts()));
		return jsonAppUser;
	}

	@Override
	public AppUser convertToEntity(JsonAppUser jsonAppUser) {
		AppUser appUser = new AppUser();
		appUser.setEmail(jsonAppUser.getEmail());
		appUser.setUserName(jsonAppUser.getUserName());
		appUser.setName(jsonAppUser.getName());
		appUser.setSurname(jsonAppUser.getSurname());
		return appUser;
	}


}
