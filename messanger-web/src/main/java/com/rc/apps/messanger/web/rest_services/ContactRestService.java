package com.rc.apps.messanger.web.rest_services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.domain.commons.LoggingUtil;
import com.rc.apps.messanger.logic.services.AppUserService;
import com.rc.apps.messanger.logic.services.ContactService;
import com.rc.apps.messanger.web.converters.AppUserConverter;
import com.rc.apps.messanger.web.converters.ContactConverter;
import com.rc.apps.messanger.web.converters.PojoConverter;
import com.rc.apps.messanger.web.json_model.JsonAppUser;
import com.rc.apps.messanger.web.json_model.JsonContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
public class ContactRestService {

    @Autowired
    private ContactService contactService;

    private PojoConverter<Contact, JsonContact> contactConverter = new ContactConverter();

    @RequestMapping(value = "/allContactsOfUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JsonContact> getAllContactsForUser(@RequestParam("userEmail") String userEmail) {
        List<Contact> contacts = contactService.getAllContactsOfUser(userEmail);
        List<JsonContact> jsonContacts = contactConverter.toJsonsList(contacts);
        return jsonContacts;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<JsonContact> addContactToUser(@RequestParam("userEmail") String userEmail, @RequestBody String userEmailToAdd) {
        System.out.println("addContactForUser: PARAMS: " + userEmail + ", contact to be added (string): " + userEmailToAdd);
        List<JsonContact> jsonContacts = contactConverter.toJsonsList(
                contactService.addContactToUser(userEmail, userEmailToAdd)
        );
        return jsonContacts;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<JsonContact> removeContactFromUser(@RequestParam("userEmail") String userEmail, @RequestBody String userEmailToRemove) {
        System.out.println("removeContactFromUser: PARAMS: " + userEmail + ", contact to be removed (string): " + userEmailToRemove);
        List<JsonContact> jsonContacts = contactConverter.toJsonsList(
                contactService.removeContactFromUser(userEmail, userEmailToRemove)
        );
        return jsonContacts;
    }


}
