package com.rc.apps.messanger.web.json_model.responses;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundResponse extends ErrorResponse {

	private String resourceIdentifier;

	public ResourceNotFoundResponse(String resourceIdentifier) {
		super(HttpStatus.NOT_FOUND);
		this.resourceIdentifier = resourceIdentifier;
	}
	
	public String getResourceIdentifier() {
		return resourceIdentifier;
	}

	public void setResourceIdentifier(String resourceIdentifier) {
		this.resourceIdentifier = resourceIdentifier;
	}
	
}
