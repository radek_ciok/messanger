package com.rc.apps.messanger.web.converters;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.domain.Contact;
import com.rc.apps.messanger.web.json_model.JsonContact;

import java.util.List;

public class ContactConverter extends PojoConverter<Contact, JsonContact>  {
    @Override
    protected JsonContact convertToJson(Contact contact) {
        JsonContact jsonContact = new JsonContact();
        jsonContact.setEmail(contact.getEmail());
        jsonContact.setUserName(contact.getUserName());
        jsonContact.setName(contact.getName());
        jsonContact.setSurname(contact.getSurname());
        return jsonContact;
    }

    @Override
    protected Contact convertToEntity(JsonContact jsonContact) {
        return null;
    }

}
