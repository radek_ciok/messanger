package com.rc.apps.messanger.web.rest_services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rc.apps.messanger.domain.AppUser;
import com.rc.apps.messanger.logic.exceptions.ResourceNotFoundException;
import com.rc.apps.messanger.logic.services.AppUserService;
import com.rc.apps.messanger.web.converters.AppUserConverter;
import com.rc.apps.messanger.web.json_model.JsonAppUser;
import com.rc.apps.messanger.web.json_model.responses.ResourceNotFoundResponse;

@RestController
@CrossOrigin("http://localhost:4200")
public class AppUserRestService {
	
	private Logger logger = LoggerFactory.getLogger(AppUserRestService.class);

	@Autowired
	private AppUserService appUserService;
	
	private AppUserConverter appUserConverter = new AppUserConverter();
	
	@RequestMapping(value = "/allAppUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<JsonAppUser> getAllUsers() {
		logger.info("Getting all users");
		return appUserConverter.toJsonsList(appUserService.getAllUsers());
	}

	@RequestMapping(value = "/appUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonAppUser getAppUserByEmail(@RequestParam("email") String email) {
		AppUser appUser = (AppUser) chechNull(appUserService.getUserByEmail(email), email);
		return appUserConverter.toJson(appUser);
	}
	
	@RequestMapping(value = "/appUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<JsonAppUser> storeAppUser(@RequestBody JsonAppUser jsonAppUser) {
		chechNull(jsonAppUser, jsonAppUser.getEmail());
		AppUser appUserToStore = appUserConverter.toEntity(jsonAppUser);

		List<AppUser> storedAppUser = appUserService.storeAppUser(appUserToStore);

		List<JsonAppUser> storedJsonAppUser = appUserConverter.toJsonsList(storedAppUser);
		return storedJsonAppUser;
	}
	
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object handleException(ResourceNotFoundException exception) {
        return new ResourceNotFoundResponse(exception.getResourceIdentifier());
    }
    
    private Object chechNull(Object obj, String identifier) {
    	if (obj == null) {
    		throw new ResourceNotFoundException(identifier);
    	}
    	return obj;
    }
    
}
