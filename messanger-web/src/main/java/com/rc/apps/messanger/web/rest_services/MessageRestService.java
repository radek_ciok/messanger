package com.rc.apps.messanger.web.rest_services;

import com.rc.apps.messanger.domain.MessageQueue;
import com.rc.apps.messanger.messaging.MessageHandler;
import com.rc.apps.messanger.messaging.conversion.MessageConverter;
import com.rc.apps.messanger.messaging.model.JsonMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
//@CrossOrigin({"http://localhost:4200", "http://localhost:8081"})
public class MessageRestService {

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/message", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public JsonMessage sendMessage(@RequestBody JsonMessage message) {
        System.out.println("Handling message: " + message);
        return messageHandler.handleMessageFromUser(message);
    }

}
