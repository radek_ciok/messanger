package com.rc.apps.messanger.web.converters;

import java.util.ArrayList;
import java.util.List;

public abstract class PojoConverter<E, J> {

	public J toJson(E entity) {
		return entity == null ? null : convertToJson(entity);
	}

	public E toEntity(J json) {
		return json == null ? null : convertToEntity(json);
	}
	
	protected abstract J convertToJson(E entity);

	protected abstract E convertToEntity(J json);
	
	
	public List<J> toJsonsList(List<E> entities) {
		List<J> jsonsList = new ArrayList<J>();
		for (E entity : entities) {
			jsonsList.add(toJson(entity));
		}
		return jsonsList;
	}
	
}
