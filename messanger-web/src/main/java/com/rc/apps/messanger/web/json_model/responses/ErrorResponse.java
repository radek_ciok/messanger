package com.rc.apps.messanger.web.json_model.responses;

import org.springframework.http.HttpStatus;

public class ErrorResponse {

	private HttpStatus httpStatus;

	public ErrorResponse(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
	
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	
}
