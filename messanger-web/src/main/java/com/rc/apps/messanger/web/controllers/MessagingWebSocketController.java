package com.rc.apps.messanger.web.controllers;

import com.rc.apps.messanger.domain.MessageQueue;
import com.rc.apps.messanger.messaging.model.JsonMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessagingWebSocketController {

    @MessageMapping("/message")
    @SendTo("/topic/messages")
    public JsonMessage handleMessages(JsonMessage message) throws Exception {
        System.out.println("Handling message: " + message.getText());
        return message;
    }

}
