package com.rc.apps.messanger.web.json_model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

//@Data
@JsonSerialize
public class JsonAppUser {

	@NotNull
	private String email;
		
	private String name;
	
	private String surname;
	
	private String userName;

	private List<JsonContact> contacts;

	public JsonAppUser() {
		super();
	}

	public JsonAppUser(String email, String name, String surname, String userName) {
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.userName = userName;
	}

	public static JsonAppUser createInstance(String email, String name, String surname, String userName) {
		return new JsonAppUser(email, name, surname, userName);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<JsonContact> getContacts() {
		return contacts;
	}

	public void setContacts(List<JsonContact> contacts) {
		this.contacts = contacts;
	}
}
