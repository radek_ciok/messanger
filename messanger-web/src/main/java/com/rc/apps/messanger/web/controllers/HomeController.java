package com.rc.apps.messanger.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rc.apps.messanger.domain.Health;
import com.rc.apps.messanger.logic.services.HealthService;

@Controller
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private HealthService healthService;
	
    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap model){
    	System.out.println("Hangling request Messanger Home Page");
    	System.out.println("is service alive: " + healthService.isAlive());
    	
    	Health h = new Health();
    	h.setIsAlive(true);
    	
    	System.out.println("health entity: " + h);
    	
        return "index";
    }

}