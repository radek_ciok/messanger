package com.rc.apps.messanger.messaging.conversion;

import com.rc.apps.messanger.domain.Message;
import com.rc.apps.messanger.domain.MessageHistory;
import com.rc.apps.messanger.domain.MessageQueue;
import com.rc.apps.messanger.messaging.model.JsonMessage;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MessageConverter {

    public JsonMessage toJson(Message message) {
        JsonMessage jsonMessage = new JsonMessage();
        jsonMessage.setText(message.getText());
        jsonMessage.setRecipients(stringToList(message.getRecipients()));
        return jsonMessage;
    }

    public Message toEntityMessageQueue(JsonMessage jsonMessage) {
        Message message = new MessageQueue();
        message.setText(jsonMessage.getText());
        message.setRecipients(listToString(jsonMessage.getRecipients()));
        return message;
    }

    public Message toEntityMessageHistory(JsonMessage jsonMessage) {
        Message message = new MessageHistory();
        message.setText(jsonMessage.getText());
        message.setRecipients(listToString(jsonMessage.getRecipients()));
        return message;
    }

    private String listToString(List<String> list) {
        return list.stream().collect(Collectors.joining(";"));
    }
    private List<String> stringToList(String string) {
        return Arrays.asList(string.split(";"));
    }
}
