package com.rc.apps.messanger.messaging.channel_jms;

import com.rc.apps.messanger.messaging.MessageHandler;
import com.rc.apps.messanger.messaging.configuration.JmsConfig;
import com.rc.apps.messanger.messaging.model.JsonMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@Profile("channel-jms")
public class MessageHandlerJMSChannel implements MessageHandler {

    @Autowired
    private JmsTemplate jmsTemplate;

    //TODO: use configured destination name
    @JmsListener(destination = "${jms.queue.1}")
    public void handleMessagesToUser(JsonMessage jsonMessage) {
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("Response received : " + jsonMessage);
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++");

        //TODO: store message in history if success
        // store message in failed if faile


    }

    @Override
    public JsonMessage handleMessageFromUser(JsonMessage jsonMessage) {
        jmsTemplate.convertAndSend(jsonMessage);

        //TODO: handle queue unavailable

        return jsonMessage;
    }

}

