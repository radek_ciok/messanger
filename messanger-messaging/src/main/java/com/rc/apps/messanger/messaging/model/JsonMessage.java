package com.rc.apps.messanger.messaging.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class JsonMessage implements Serializable {

    private String text;

    private Date sendTime;

    private Date receiveTime;

    private List<String> recipients;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Date receiveTime) {
        this.receiveTime = receiveTime;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    @Override
    public String toString() {
        return "JsonMessage{" +
                "text='" + text + '\'' +
                ", sendTime=" + sendTime +
                ", receiveTime=" + receiveTime +
                ", recipients=" + recipients +
                '}';
    }
}
