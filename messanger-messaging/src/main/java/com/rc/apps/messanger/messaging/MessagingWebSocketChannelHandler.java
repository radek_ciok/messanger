package com.rc.apps.messanger.messaging;

import com.rc.apps.messanger.messaging.model.JsonMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessagingWebSocketChannelHandler {

    private final String WEB_SOCKET_TOPIC = "/topic/messages";

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    public void sendMessageToUser(JsonMessage jsonMessage) {
        messagingTemplate.convertAndSend(WEB_SOCKET_TOPIC, jsonMessage);
    }

}
