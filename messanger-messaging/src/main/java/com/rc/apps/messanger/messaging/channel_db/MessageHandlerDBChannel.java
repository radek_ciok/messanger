package com.rc.apps.messanger.messaging.channel_db;

import com.rc.apps.messanger.domain.Message;
import com.rc.apps.messanger.domain.MessageHistory;
import com.rc.apps.messanger.domain.MessageQueue;
import com.rc.apps.messanger.logic.services.MessageService;
import com.rc.apps.messanger.messaging.MessageHandler;
import com.rc.apps.messanger.messaging.MessagingWebSocketChannelHandler;
import com.rc.apps.messanger.messaging.conversion.MessageConverter;
import com.rc.apps.messanger.messaging.model.JsonMessage;
import com.rc.apps.messanger.repository.MessageHistoryRepository;
import com.rc.apps.messanger.repository.MessageQueueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Profile("channel-db")
public class MessageHandlerDBChannel implements MessageHandler {

    @Autowired
    private MessageService messageService;

    private MessageConverter messageConverter = new MessageConverter();

    @Autowired
    private MessagingWebSocketChannelHandler wsChannelHandler;

    @Override
    @Scheduled(fixedDelay = 1000)
    public void handleMessagesToUser(JsonMessage jsonMessage) {
        List<Message> messages = messageService.getAllMessageQueues();
        for (Message message : messages) {
            handleMessageToUser(message);
        }
    }


    @Transactional
    public void handleMessageToUser(Message message) {
        JsonMessage jsonMessage = messageConverter.toJson(message);
        wsChannelHandler.sendMessageToUser(jsonMessage);
        messageService.moveMessageFromQueueToHistory((MessageQueue) message); //TODO: do something with mess tith types
    }

    @Override
    @Transactional
    public JsonMessage handleMessageFromUser(JsonMessage jsonMessage) {
        Message messageQueue = messageConverter.toEntityMessageQueue(jsonMessage);
        return messageConverter.toJson(messageService.saveMessage(messageQueue));
    }

}
