package com.rc.apps.messanger.messaging;

import com.rc.apps.messanger.domain.MessageQueue;
import com.rc.apps.messanger.messaging.conversion.MessageConverter;
import com.rc.apps.messanger.messaging.model.JsonMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;

public interface MessageHandler {

    public JsonMessage handleMessageFromUser(JsonMessage jsonMessage);

    public void handleMessagesToUser(JsonMessage jsonMessage);


}
