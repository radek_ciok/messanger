import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServerService {

  constructor(private http: Http) {}

  storeAppUser(appUser) {
    const headers = new Headers({'Content-Type': 'application/json'});
    return this.http.post('http://localhost:8081/messanger-xx/appUser', appUser, {headers: headers})
      .map(
        (response: Response) => response.json()
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Cannot save user.');
        }
      );
  }

  getAllAppUsers() {
    return this.http.get('http://localhost:8081/messanger-xx/allAppUsers')
      .map(
        (response: Response) => {
          const users = response.json();
          for (const user of users) {
            // do something with a user
          }
          return users;
        }
      )
      .catch(
        (error: Response) => {
          return Observable.throw('Cannot get all users.');
        }
      );
  }

}
