import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { Response } from '@angular/http';

import { ServerService } from './server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild('messageTextInput')
  private messageTextInput: ElementRef;

  @ViewChild('userNameInput')
  private userNameInput: ElementRef;
  @ViewChild('emailInput')
  private emailInput: ElementRef;
  @ViewChild('nameInput')
  private nameInput: ElementRef;
  @ViewChild('surnameInput')
  private surnameInput: ElementRef;

  appName = 'Messanger';
  messages = [
    {
      direction: 'out',
      receiver: 'rec 1',
      time: null,
      text: 'message 1'
    },
    {
      direction: 'in',
      receiver: 'rec 1',
      time: null,
      text: 'message 2'
    }
  ];

  users = [];

  contacts = [];

  receivers = [];

  constructor(private serverService: ServerService) {}

  ngOnInit() {
    this.getAllUsers();
  }

  onSendMessage() {

    const text = this.extractMessageText();

    const message = {
      direction: 'out',
      receiver: 'rec 1',
      time: null,
      text: text
    };
    this.messages.push(message);
    this.clearMessageText();
  }

  private getAllUsers() {
    this.serverService.getAllAppUsers()
      .subscribe(
        (users: any[]) => this.users = users,
        (error) => console.log(error)
      );
  }

  onAddUser() {

    const appUser = this.extractUser();

    this.serverService.storeAppUser(appUser)
      .subscribe(
        (newUser: any) => this.users.push(newUser),
        (error) => console.log(error)
      );

    this.clearUser();
  }

  onAddUserToContacts($event, index) {
    if (!this.users[index].selected) {
      this.users[index].selected = true;
      this.contacts.push(this.users[index]);
    }
  }

  onRemoveContact(idx) {
    const userName = this.contacts[idx].userName;
    const index = this.findIndexByName(this.contacts, userName);
    this.receivers.splice(index, 1);
    this.contacts.splice(idx, 1);
  }

  onSelectContact(idx) {
    this.receivers.push( this.contacts[idx] );
  }

  private findIndexByName(list, userName): number {
    for (let i = 0; i < list.length; i++) {
      if (userName === list.userName ) {
        return i;
      }
    }
    return -1;
  }

  private extractMessageText(): string {
    return this.messageTextInput.nativeElement.innerText;
  }

  private clearMessageText() {
    this.messageTextInput.nativeElement.innerText = '';
  }

  private extractUser(): any {
    const appUser = {
        userName: this.userNameInput.nativeElement.value,
        email: this.emailInput.nativeElement.value,
        name: this.nameInput.nativeElement.value,
        surname: this.surnameInput.nativeElement.value
      }
    return appUser;
  }

  private clearUser() {
    this.userNameInput.nativeElement.value = '';
    this.emailInput.nativeElement.value = '';
    this.nameInput.nativeElement.value = '';
    this.surnameInput.nativeElement.value = '';
  }

  styleSelected(user) {
    if (user.selected) {
      return 'button_selected';
    } else {
      return 'button_not_selected';
    }
  }

}
